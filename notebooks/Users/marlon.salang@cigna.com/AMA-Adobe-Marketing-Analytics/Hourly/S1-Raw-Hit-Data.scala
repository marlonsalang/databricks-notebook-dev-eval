// Databricks notebook source
import org.apache.avro.Schema
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions.{col, current_timestamp, lit, to_date, to_timestamp, _}

import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.databricks.spark.avro.SchemaConverters

val CLIENT = AmazonS3ClientBuilder.defaultClient

// COMMAND ----------

//Variable Mapping
 val variable_map_df = spark.read
             .format("csv")
             .option("delimiter",",")
             .option("header", "true")             
             .option("inferSchema", true)             
             .load("s3a://s3-digexp-sdbx-adobework/variable_mappings/adobe_variable_map.csv")

//Event Mapping
 val event_map_df = spark.read
             .format("csv")
             .option("delimiter",",")
             .option("header", "true")             
             .option("inferSchema", true)             
             .load("s3a://s3-digexp-sdbx-adobework/variable_mappings/adobe_event_map.csv")

// COMMAND ----------

val browser_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/Browser-20181001-v1.avsc").getObjectContent
val browser_schema_input_stream = new Schema.Parser().parse(browser_s3_content)
val browser_struct_type = SchemaConverters.toSqlType(browser_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${browser_schema_input_stream.toString(true)}
           |""".stripMargin)
}

browser_s3_content.close

val browser_bc_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(browser_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/browser.tsv")

display(browser_bc_df)

// COMMAND ----------

val browser_type_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/BrowserType-20181001-v1.avsc").getObjectContent
val browser_type_schema_input_stream = new Schema.Parser().parse(browser_type_s3_content)
val browser_type_struct_type = SchemaConverters.toSqlType(browser_type_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${browser_type_schema_input_stream.toString(true)}
           |""".stripMargin)
}

browser_type_s3_content.close

val browser_type_bc_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(browser_type_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/browser_type.tsv")

display(browser_type_bc_df)

// COMMAND ----------

val color_depth_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/ColorDepth-20181001-v1.avsc").getObjectContent
val color_depth_schema_input_stream = new Schema.Parser().parse(color_depth_s3_content)
val color_depth_struct_type = SchemaConverters.toSqlType(color_depth_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${color_depth_schema_input_stream.toString(true)}
           |""".stripMargin)
}

color_depth_s3_content.close

val color_depth_bc_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(color_depth_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/color_depth.tsv")

display(color_depth_bc_df)

// COMMAND ----------

val connection_type_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/ConnectionType-20181001-v1.avsc").getObjectContent
val connection_type_schema_input_stream = new Schema.Parser().parse(connection_type_s3_content)
val connection_type_struct_type = SchemaConverters.toSqlType(connection_type_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${connection_type_schema_input_stream.toString(true)}
           |""".stripMargin)
}

connection_type_s3_content.close

val connection_type_bc_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(connection_type_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/connection_type.tsv")

display(connection_type_bc_df)

// COMMAND ----------

val country_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/Country-20181001-v1.avsc").getObjectContent
val country_schema_input_stream = new Schema.Parser().parse(country_s3_content)
val country_struct_type = SchemaConverters.toSqlType(country_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${country_schema_input_stream.toString(true)}
           |""".stripMargin)
}

country_s3_content.close

val country_bc_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(country_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/country.tsv")

display(country_bc_df)

// COMMAND ----------

val event_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/Event-20181001-v1.avsc").getObjectContent
val event_schema_input_stream = new Schema.Parser().parse(event_s3_content)
val event_struct_type = SchemaConverters.toSqlType(event_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${event_schema_input_stream.toString(true)}
           |""".stripMargin)
}

event_s3_content.close

val event_bc_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(event_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/event.tsv")

display(event_bc_df)

// COMMAND ----------

val javascript_version_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/JavascriptVersion-20181001-v1.avsc").getObjectContent
val javascript_version_schema_input_stream = new Schema.Parser().parse(javascript_version_s3_content)
val javascript_version_struct_type = SchemaConverters.toSqlType(javascript_version_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${javascript_version_schema_input_stream.toString(true)}
           |""".stripMargin)
}

javascript_version_s3_content.close

val javascript_version_bc_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(javascript_version_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/javascript_version.tsv")

display(javascript_version_bc_df)

// COMMAND ----------

val languages_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/Languages-20181001-v1.avsc").getObjectContent
val languages_schema_input_stream = new Schema.Parser().parse(languages_s3_content)
val languages_struct_type = SchemaConverters.toSqlType(languages_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${languages_schema_input_stream.toString(true)}
           |""".stripMargin)
}

languages_s3_content.close

val languages_bc_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(languages_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/languages.tsv")

display(languages_bc_df)

// COMMAND ----------

val operating_systems_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/OperatingSystems-20181001-v1.avsc").getObjectContent
val operating_systems_schema_input_stream = new Schema.Parser().parse(operating_systems_s3_content)
val operating_systems_struct_type = SchemaConverters.toSqlType(operating_systems_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${operating_systems_schema_input_stream.toString(true)}
           |""".stripMargin)
}

operating_systems_s3_content.close

val operating_systems_bc_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(operating_systems_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/operating_systems.tsv")

display(operating_systems_bc_df)

// COMMAND ----------

val plugins_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/Plugins-20181001-v1.avsc").getObjectContent
val plugins_schema_input_stream = new Schema.Parser().parse(plugins_s3_content)
val plugins_struct_type = SchemaConverters.toSqlType(plugins_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${plugins_schema_input_stream.toString(true)}
           |""".stripMargin)
}

plugins_s3_content.close

val plugins_bc_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(plugins_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/plugins.tsv")

display(plugins_bc_df)

// COMMAND ----------

val referrer_type_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/ReferrerType-20181001-v1.avsc").getObjectContent
val referrer_type_schema_input_stream = new Schema.Parser().parse(referrer_type_s3_content)
val referrer_type_struct_type = SchemaConverters.toSqlType(referrer_type_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${referrer_type_schema_input_stream.toString(true)}
           |""".stripMargin)
}

referrer_type_s3_content.close

val referrer_type_bc_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(referrer_type_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/referrer_type.tsv")

display(referrer_type_bc_df)

// COMMAND ----------

val resolution_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/Resolution-20181001-v1.avsc").getObjectContent
val resolution_schema_input_stream = new Schema.Parser().parse(resolution_s3_content)
val resolution_struct_type = SchemaConverters.toSqlType(resolution_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${resolution_schema_input_stream.toString(true)}
           |""".stripMargin)
}

resolution_s3_content.close

val resolution_bc_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(resolution_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/resolution.tsv")

display(resolution_bc_df)

// COMMAND ----------

val search_engines_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/SearchEngines-20181001-v1.avsc").getObjectContent
val search_engines_schema_input_stream = new Schema.Parser().parse(search_engines_s3_content)
val search_engines_struct_type = SchemaConverters.toSqlType(search_engines_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${search_engines_schema_input_stream.toString(true)}
           |""".stripMargin)
}

search_engines_s3_content.close

val search_engines_bc_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(search_engines_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/search_engines.tsv")

display(search_engines_bc_df)

// COMMAND ----------

val raw_hits_s3_content = CLIENT.getObject("s3-digexp-sdbx-adobework", "avro_schemas/HitData-20181001-v1.avsc").getObjectContent
val raw_hits_schema_input_stream = new Schema.Parser().parse(raw_hits_s3_content)
val raw_hits_struct_type = SchemaConverters.toSqlType(raw_hits_schema_input_stream).dataType match {
  case s:StructType => s
  case _ => throw new RuntimeException(
        s"""Avro schema cannot be converted to a Spark SQL StructType:
           |
           |${raw_hits_schema_input_stream.toString(true)}
           |""".stripMargin)
}

raw_hits_s3_content.close

val hit_data_df = spark
                    .read
                    .option("delimiter", "\t")
                    .option("header", "false")
                    .schema(raw_hits_struct_type)
                    .format("csv")
                    .load("s3a://s3-digexp-sdbx-adobeunzip/hit_data.tsv")

val rawHits_processsedDF = hit_data_df
  .join(broadcast(search_engines_bc_df), col("search_engine") === col("search_engine_id"), "left")
  .join(broadcast(plugins_bc_df), col("plugins") === col("plugin_id"), "left")
  .join(broadcast(operating_systems_bc_df), col("os") === col("operating_system_id"), "left")
  .join(broadcast(languages_bc_df), col("language") === col("language_id"), "left")
  .join(broadcast(javascript_version_bc_df), col("javascript") === col("javascript_version_id"), "left")
  .join(broadcast(country_bc_df), col("country") === col("country_id"), "left")
  .join(broadcast(connection_type_bc_df), col("connection_type") === col("connection_type_id"), "left")
  .join(broadcast(browser_type_bc_df), col("browser") === col("browser_type_id"), "left")
  .join(broadcast(browser_bc_df), col("browser") === col("browser_id"), "left")
  .join(broadcast(color_depth_bc_df), col("color") === col("color_depth_id"), "left")
  .withColumn("session_id", concat(col("post_visid_high"), col("post_visid_low"), col("visit_num"), col("visit_start_time_gmt")))
  .withColumn("hit_id", concat(col("hitid_high"), col("hitid_low")))
  .withColumn("visitor_id", concat(col("post_visid_high"), col("post_visid_low")))
  .withColumn("record_type", when($"post_page_url".isNotNull, "pageview")
    .when($"post_pagename".isNotNull, "pageview")
    .when($"post_page_event" === lit("100"), "action")
    .when($"post_page_event" === lit("101"), "action_download")
    .when($"post_page_event" === lit("102"), "action_exit")
    .otherwise("action_unknown"))
  .withColumn("date_time", unix_timestamp($"date_time", "yyyy-MM-dd HH:mm:ss").cast(TimestampType).as("timestamp"))
  .withColumn( "dd_hh", substring(($"date_time"), 1,13).as("dd_hh"))
  .withColumn("weekly_visitor", $"weekly_visitor".cast(IntegerType).as("weekly_visitor"))
  .withColumn("yearly_visitor", $"yearly_visitor".cast(IntegerType).as("yearly_visitor"))
  .withColumn("daily_visitor", $"daily_visitor".cast(IntegerType).as("daily_visitor"))
  .withColumn("monthly_visitor", $"monthly_visitor".cast(IntegerType).as("monthly_visitor"))
  .withColumn("hourly_visitor", $"hourly_visitor".cast(IntegerType).as("hourly_visitor"))
  .withColumn("quarterly_visitor", $"quarterly_visitor".cast(IntegerType).as("quarterly_visitor"))
  .withColumn("visit_page_num", $"visit_page_num".cast(IntegerType).as("visit_page_num"))

// HOURLY FILEs WRITE TO:  s3a://s3-digexp-{ENV}-adobework/yyyy-mm-dd/
// DAILY ROLLUP job will read all from: s3a://s3-digexp-{ENV}-adobework/dd_hh=yyyy-mm-dd hh/
// DAILY ROLLUP output will : s3a://s3-digexp-{ENV}-adobefinal/adobe/year=2018/month=10/day=12/part-*.snappy.parquet
rawHits_processsedDF
  .coalesce(1)
  .write
  .partitionBy("dd_hh")
  .mode(SaveMode.Overwrite)
  .parquet("s3a://s3-digexp-sdbx-adobework/adbparquet/adb_hits")

// COMMAND ----------

dbutils.notebook.exit("s3a://s3-digexp-sdbx-adobework/adbparquet/adb_hits")