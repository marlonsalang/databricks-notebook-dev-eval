// Databricks notebook source
val timeoutInSec = 60

val hitsPath = dbutils.notebook.run("./S1-Raw-Hit-Data", timeoutInSec)
println("exited S1.hitsPath: '" + hitsPath + "'")

val s2 = dbutils.notebook.run("./S2-Stage-Data", timeoutInSec, Map("hitsPath" -> hitsPath))
println("exited S2.exitValue: '" + s2 + "'")

// val s3 = dbutils.notebook.run("./S3-Aggregate-Data", timeoutInSec, Map("hitsPath" -> hitsPath))
// println("exited S3.exitValue: '" + s3 + "'")