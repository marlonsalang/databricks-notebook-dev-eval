// Databricks notebook source
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions.{col, current_timestamp, lit, to_date, to_timestamp, _}
import org.apache.spark.sql.expressions.Window
import spark.implicits._

// COMMAND ----------

import org.apache.spark.sql.functions.col

println("Starting Stg2-Cmd2...")
val hitsPath = dbutils.widgets.get("hitsPath")
println("S2.hitsPath->" + hitsPath)

val mappingDf = spark.read.option("header", "true").csv("s3a://s3-digexp-sdbx-adobework/variable_mappings/adobe_variable_map.csv")
var hitsPageviewsRef = mappingDf.filter($"pageviews" === "x").select($"adobe_hits_reference").map(r => r.getString(0)).collect()
var hitsPageviewsMap = mappingDf.filter($"pageviews" === "x").select($"adobe_hits_reference", $"fieldname").as[(String, String)].collect.toMap
val pageviewsColNames = hitsPageviewsRef.map(name => col(name))

//val hitDF = sqlContext.read.parquet("s3a://s3-digexp-sdbx-adobework/adbparquet/adb_hits")
val hitDF = sqlContext.read.parquet(hitsPath)
val selectedPageviewsDf = hitDF.select(pageviewsColNames:_*)

val pageviews_2DF = selectedPageviewsDf
          .select(selectedPageviewsDf.columns.map(c => col(c).as(hitsPageviewsMap.getOrElse(c, c))): _*)
          .filter($"record_type" === "pageview")
          .filter($"exclude_hit" === "0")
          .filter(not($"hit_source" isin ("5", "7", "8", "9")))

// COMMAND ----------

var hitsActionsRef = mappingDf.filter($"actions" === "x").select($"adobe_hits_reference").map(r => r.getString(0)).collect()

var hitsActionsMap = mappingDf.filter($"actions" === "x").select($"adobe_hits_reference", $"fieldname").as[(String, String)].collect.toMap

val actionsColNames = hitsActionsRef.map(name => col(name))

val selectedActionsDf = hitDF.select(actionsColNames:_*)

val actions_2DF = selectedActionsDf
          .select(selectedActionsDf.columns.map(c => col(c).as(hitsActionsMap.getOrElse(c, c))): _*)
          .withColumn("pagename_trimmed", substring_index(col("pagename"), ": page", 1))
          .filter($"record_type" like "action%")
          .filter($"exclude_hit" === "0")
          .filter(not($"hit_source" isin ("5", "7", "8", "9")))

// COMMAND ----------

val hit_event_list_2_1DF = hitDF
          .withColumn("event_list", split($"post_event_list", ","))
          .withColumn("event", explode($"event_list"))
          .withColumn("event_id", split($"event","=").getItem(0))
          .withColumn("event_value", split($"event","=").getItem(1))
          .withColumn("hit_event_index", row_number().over(Window.partitionBy(col("hit_id")).orderBy(col("session_id").asc)))
          .filter($"event_id" > "199")
          .select("session_id"
          , "hit_id"
          , "visit_page_num"
          , "date_time"
          , "event_id"
          , "event_value"
          , "hit_event_index"
          )

// COMMAND ----------

val session_aggregateDF =  hitDF
        .groupBy(
            "session_id"
          )
        .agg(
            to_timestamp(max("date_time")).alias("date_time_max")
              ,to_timestamp(min("date_time")).alias("date_time_min")
              , (unix_timestamp(to_timestamp(max("date_time"))) -  unix_timestamp(to_timestamp(min("date_time")))).alias("time_on_site")
              , sum(when($"record_type" === "pageview", "1")).alias("pageview_count").as[Long]
              , sum(when($"record_type" === "pageview", "0").otherwise("1")).alias("action_count").as[Long]
              , substring(min("date_time"), 1,10).alias("dd")
              , substring(min("date_time"), 1,7).alias("mm")
              , substring(min("date_time"), 1,4).alias("yy")
              , substring(min("date_time"), 1,13).alias("dd_hh")
          )

// COMMAND ----------

val product_listDF = hitDF
    .select("hit_id"
    , "session_id"
    , "post_product_list")

// COMMAND ----------

val product_list_explodeDF = hitDF
          .withColumn("product_list", split($"post_product_list", ","))
          .withColumn("products", explode($"product_list"))
          .filter($"products".like(";businessSegments%"))
          .withColumn("products1", regexp_replace($"products", ";businessSegments;;;;", ""))
          .withColumn("products2", regexp_replace($"products1", "129=", ""))          
          .withColumn("products3", split($"products2", "\\|").getItem(0))
          .withColumn("a", split($"products3", ":").getItem(0))
          .withColumn("b", split($"products3", ":").getItem(1))
          .withColumn("c", split($"products3", ":").getItem(2))
          .withColumn("d", split($"products3", ":").getItem(3))
          .withColumn("role",  when($"a" === "role", $"b").otherwise(""))
          .withColumn("platform",  when($"a" === "platform", $"b").otherwise(""))
          .withColumn("status",  when($"a" === "status", $"b").otherwise(""))      
          .withColumn("client_type",  when($"a" === "clientType", $"b").otherwise(""))  
          .withColumn("has_one_guide",  when($"a" === "hasOneGude", $"b").otherwise(""))  
          .withColumn("has_v2v",  when($"a" === "hasV2v", $"b").otherwise(""))  
          .withColumn("tps_status",  when($"a" === "tpsStatus", $"b").otherwise(""))  
          .withColumn("appGroup",  when($"a" === "appGroup", $"b").otherwise(""))            
          .withColumn("product_group_type_dental",  when($"a" === "productGroupType" && $"b" === "Dental", concat($"d", lit(":"), $"c")).otherwise(""))
          .withColumn("product_group_type_medical",  when($"a" === "productGroupType" && $"b" === "Medical", concat($"d", lit(":"), $"c")).otherwise(""))
          .withColumn("product_group_type_healthwellness",  when($"a" === "productGroupType" && $"b" === "Health and Wellness", concat($"d", lit(":"), $"c")).otherwise("")) 
          .withColumn("product_group_type_pharmacy",  when($"a" === "productGroupType" && $"b" === "Pharmacy", $"c").otherwise(""))     
          .withColumn("product_group_type_behavioral",  when($"a" === "productGroupType" && $"b" === "Behavioral", $"c").otherwise(""))           
          .groupBy("session_id")
          .agg(
           max("role").alias("role")
           , max("platform").alias("platform")
           , max("status").alias("status")
           , max("client_type").alias("client_type")
           , max("has_one_guide").alias("hase_one_guide")
           , max("has_v2v").alias("has_v2v")
           , max("tps_status").alias("tps_status")
           , max("product_group_type_dental").alias("product_group_type_dental")
           , max("product_group_type_medical").alias("product_group_type_medical")
           , max("product_group_type_medical").alias("product_group_type_medical")           
           , max("product_group_type_healthwellness").alias("product_group_type_healthwellness")           
           , max("product_group_type_pharmacy").alias("product_group_type_pharmacy")
           , max("product_group_type_behavioral").alias("product_group_type_behavioral") 
           , collect_set("appGroup").alias("app_groups")
           )

// COMMAND ----------

// val hitsPath = dbutils.widgets.get("hitsPath")
// println("S2.hitsPath->" + hitsPath)
// val hitDF = sqlContext.read.parquet(hitsPath)

val session_aggregateDF =  hitDF
        .groupBy(
            "session_id"
          )
        .agg(
            to_timestamp(max("date_time")).alias("date_time_max")
              ,to_timestamp(min("date_time")).alias("date_time_min")
              , (unix_timestamp(to_timestamp(max("date_time"))) -  unix_timestamp(to_timestamp(min("date_time")))).alias("time_on_site")
              , sum(when($"record_type" === "pageview", "1")).alias("pageview_count").as[Long]
              , sum(when($"record_type" === "pageview", "0").otherwise("1")).alias("action_count").as[Long]
              , substring(min("date_time"), 1,10).alias("dd")
              , substring(min("date_time"), 1,7).alias("mm")
              , substring(min("date_time"), 1,4).alias("yy")
              , substring(min("date_time"), 1,13).alias("dd_hh")
          )

// COMMAND ----------

val sessions_3DF = pageviews_2DF.as("pv")
          .join(session_aggregateDF.as("sa"), $"pv.session_id" === $"sa.session_id")
          .groupBy(
            "pv.post_visid_high"
          , "pv.post_visid_low"
          , "pv.session_id"
          , "pv.visit_num"
          , "pv.visit_start_time_gmt"
          , "pv.user_hash"
          , "pv.userid"
          , "pv.campaign"
          , "pv.carrier"
          , "pv.code_ver"
          , "pv.country"
          , "pv.domain"
          , "pv.first_hit_page_url"
          , "pv.first_hit_pagename"
          , "pv.first_hit_referrer"
          , "pv.first_hit_time_gmt"
          , "pv.geo_city"
          , "pv.geo_country"
          , "pv.geo_dma"
          , "pv.geo_region"
          , "geo_zip"
          , "pv.mcvisid"
          , "pv.mobile_id"
          , "pv.user_agent"
          , "pv.visit_referrer"
          , "pv.visit_start_page_url"
          , "pv.search_engine_name"
          , "pv.plugin_name"
          , "pv.operating_system_name"
          , "pv.language_name"
          , "pv.javascript_version_name"
          , "pv.country_name"
          , "pv.connection_type_name"
          , "pv.browser_type_name"
          , "pv.browser_name"
          , "pv.color_depth_name"
          , "sa.date_time_max"
          , "sa.date_time_min"
          , "sa.time_on_site"
          , "sa.pageview_count"
          , "sa.action_count"
          , "sa.dd"
          , "sa.mm"
          , "sa.yy"
          , "sa.dd_hh"     
          )
          .agg(to_timestamp(max("date_time")).alias("date_time_max")
              ,to_timestamp(min("date_time")).alias("date_time_min")
              , (unix_timestamp(to_timestamp(max("date_time"))) -  unix_timestamp(to_timestamp(min("date_time")))).alias("time_on_site")
              , max("enterprise_client_id").alias("enterprise_client_id") 
              , max("hourly_visitor").alias("hourly_visitor").as[Long]
              , max("daily_visitor").alias("daily_visitor").as[Long]
              , max("weekly_visitor").alias("weekly_visitor").as[Long]
              , max("monthly_visitor").alias("monthly_visitor").as[Long]
              , max("quarterly_visitor").alias("quarterly_visitor").as[Long]
              , max("yearly_visitor").alias("yearly_visitor").as[Long]
              , min(when($"login_status" === "logged in", "t").otherwise("f")).alias("has_login")
          )

// COMMAND ----------

val pageviews_3DF = pageviews_2DF
          .withColumn("pageview_sequence_index", row_number().over(Window.partitionBy(col("session_id")).orderBy(col("date_time").asc)))
          .withColumn("date_time_next_pageview", lead("date_time", 1).over(Window.partitionBy(col("session_id")).orderBy(col("date_time").asc)))
          .withColumn("pageview_id_next",lead("hit_id",1).over(Window.partitionBy(col("session_id")).orderBy(col("date_time").asc)))
          .withColumn("visit_page_num_next",lead("visit_page_num",1).over(Window.partitionBy(col("session_id")).orderBy(col("date_time").asc)))
          .withColumn("is_pageview", lit("0"))
          .withColumn("session_id_join", $"session_id")
          .select(
            "session_id"
          , "hit_id"
          , "date_time" 
          , "pageview_sequence_index"
          , "date_time_next_pageview"
          , "pageview_id_next"
          , "visit_num"
          , "visit_start_time_gmt"
          , "user_hash"
          , "userid"
          , "pagename"
          , "post_page_url"
          , "post_pagename"
          , "post_pagename_no_url"
          , "visit_page_num"
          , "visit_page_num_next"          
         )

// COMMAND ----------

val actions_3DF = actions_2DF.as("ac")
          .join(pageviews_3DF.as("pv"), $"ac.session_id" === $"pv.session_id" &&  $"ac.pagename_trimmed" === $"pv.pagename" &&  $"ac.visit_page_num" > $"pv.visit_page_num", "inner")
          .withColumn("useonlyones", row_number().over(Window.partitionBy($"ac.session_id",$"ac.pagename_trimmed",$"ac.hit_id").orderBy($"pv.visit_page_num".desc)))
          .filter($"useonlyones" === "1")
          .select(
               $"ac.session_id"
              , $"ac.visitor_id"
              , $"ac.hit_id"
              , $"pv.hit_id".alias("pageview_hit_id")
              , $"pv.visit_page_num".alias("pageview_visit_page_num")                   
              , $"ac.date_time"
              , $"ac.pagename_trimmed"
              , $"ac.visit_page_num"
              , $"ac.click_action"
              , $"ac.click_action_type"
              , $"ac.click_context"
              , $"ac.click_context_type"
              , $"ac.click_sourceid"
              , $"ac.click_tag"
              , $"ac.link_location"
              , $"ac.site_search_term"
              , $"site_search_result_count"
              , $"enterprise_client_id"
              , $"mht_provider_details"
              , $"telehealth_vendor_available"
              , $"directory_procedure_details"
              , $"facility_type"
              , $"directory_search_type_term"
              , $"tab_interactions"
              , $"provider_id"
              , $"provider_category"
              , $"provider_specialty_code"
              , $"directory_user_search_term"
              , $"directory_actual_search_term"
              , $"chat_interaction"
              , $"directory_guided_search_search_term_and_filters_selected"        
              )

// COMMAND ----------

// pageviews_3DF.write.mode(SaveMode.Overwrite).parquet("s3a://s3-digexp-sdbx-adobework/adbparquet/pageviews")
// actions_3DF.write.mode(SaveMode.Overwrite).parquet("s3a://s3-digexp-sdbx-adobework/adbparquet/actions")
// sessions_3DF.write.mode(SaveMode.Overwrite).parquet("s3a://s3-digexp-sdbx-adobework/adbparquet/sessions")
// hitDF.coalesce(1).limit(100).write.mode(SaveMode.Overwrite).parquet("s3a://s3-digexp-sdbx-adobework/adbparquet/hits")


// COMMAND ----------

dbutils.notebook.exit("S2-Agg.done")